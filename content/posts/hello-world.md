---
title: "Hello, World!"
date: 2021-10-01T20:24:48+02:00
draft: false
summary: "THE FIRST POST"
tags: ['meta']
---

Hello, I now have a blog website, yay :D

Currently the page looks white, and doesn't follow the design of the rest of
the site. This will be fixed, sometime in the future.

In the future, expect guides on different technologies.
