---
slug: daniel
title: "About Me"
date: 2022-08-01T11:12:25+02:00
draft: false
---

**Hello!**
My name's Daniel Florescu and you've found my personal website.
There isn't much currently, but you're welcome to visit my *other* website which I
use for hosting personal projects.
<https://190405.xyz>.

**Tech stack**
This website consists of statically generated HTML files using Hugo hosted on Azure.
![tech stack](/images/daniel-diagram.png)

**Personal projects**
My personal projects can be found here:

* **[GitLab/dko1905](https://gitlab.com/dko1905)**
* **[GitHub/dko1905](https://github.com/dko1905)**

**Contact info**
Email: daniel &lt;AT&gt; 0chaos &lt;DOT&gt; eu
PGP Key: [pgp-key.txt](/.well-known/pgp-key.txt)
